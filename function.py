# indent
# define a function with name
def printStr():
    print('a is an alphabetic')
    print('b is an alphabetic')

# function calling, invocation, invoke
printStr()

# Arguments, parameters
def add(n1, n2) :
    sum = n1 + n2
    #print(f'{n1} + {n2} is {sum}')
    return sum

result = add(3, 4)

# default parameter value
def cookShift(hotel, date, cook = 'Julia'):
    print('Today\'s ',  date,  ' shift is:', cook)

#cookShift('20200425', 'NY')

#print('c is an alphabetic as well', result)

# keyword arguments
cookShift(date='202004', hotel='North York', cook='Mark')

# Arbitrary arguments, Asterisk, *
def printFullName(**names):
    print('The full name is:' + names['first'] + ', ' + names['last'])

printFullName(first='Ryan', middle='Tomas', last='Wu')



