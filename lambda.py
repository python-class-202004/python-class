# small function,
# anonymous
# take any number of arguments
# can only have on express,
# return express final value
import datetime
getSum = lambda a, b: a + b
print(getSum(2, 3))

def getRetailPrice(ratio):
    return lambda rawPrice: rawPrice * ratio

myGasPrice = getRetailPrice(1.05)
myDieselPrice = getRetailPrice(1.08)
dateStr = datetime.datetime.now()
print(f'Today\'s {dateStr} prices from Esso:\n {myGasPrice(0.7)}, {myDieselPrice(0.65)}')
