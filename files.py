import os
# absolute path
# relative path
# mode flag
# format, text mode,
fd = open('./diary2.txt', 'rb')
# linux , \n
# windows, \r\n
# fd.write('line4\n\n')
# print(fd.name)
# r, read only
# r+, read and write, appending
# w, write, create a new file
# a, append, create a new file if not existed, append, increase
# x,

# format,
# t, text mode
# b, binary

# read file
"""
content = fd.read(3)
content = fd.readline()
fd.read(1)
content = fd.readline()
print(content)
# cursor,
print('current position: ', fd.tell())
fd.seek(-4, 1)
print(fd.readline())
#content = fd.readlines(3) # lists ['asd1', 'adsfads2', 'asd3']
"""
#for line in content:
#    print(line)
# fd.write('a + b is 4\n')

# get all lines
count = 0
for line in fd:
    count += 1
    #print(f"{count}:", line)
fd.close()

with open('./diary2.txt') as f:
    for line in f:
        count += 1
        print(line)

print(f.closed)



