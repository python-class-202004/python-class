# IF , else
"""
•	Equals: a == b
•	Not Equals: a != b
•	Less than: a < b
•	Less than or equal to: a <= b
•	Greater than: a > b
•	Greater than or equal to: a >= b
"""
x = 4
y = 4
if x == y:
    print(f'{x} is equal to {y}')
elif x >= 90:
    print(f'{x} is great')
elif x >= 80 and x < 90:
    print(f'{x} is good')
else:
    print('there are not equal')

# if not(x == 100):
if x != 100:
    print('')

scores = [15, 68, 70, 90]
z = 19
if z in scores:
    print('z is in scores')
else:
    print('z is not in scores')

# identity operator
# is, is not
if z is not scores:
    print('z is not scores')
