# collection
# List, 集
# square brace
#
"""
List:
 1, [ ]is a collection which
 2, is ordered
 3, changeable.
 4, Allows duplicate members.
 5, index starts from 0
"""
cars = ['benz', 'bmw', 'lexus', 'honda', 'volvo']
# print(cars[0])
# first element, last one
# cars[0] = 'gle'
# print(cars[len(cars) - 1])
# print(cars[-1])
# print(cars[1:]) # from index = 1, all the remaining elements
# print(cars[1:(3 + 1)]) # the ending is not inclusive
# cars.append('tesla')
# cars.remove('honda')
# cars.pop(2)
# cars.insert(1, 'x7')
# print(cars)

# copy list
car2020 = cars # reference
#car2020 = cars.copy()
# allocate new memory,
# assign the new address to car2020
# cars <-> car2020
car2021 = list(cars)
car2020[0] = 'gle'
print(cars[0])

cls4 = [3, 4, 7]
cls5 = [5, 3, 7]
# join two lists
clsAll = cls4 + cls5
clsAll[0] = 333
print(cls4[0])
cls4.extend(cls5)
print('count is: ', cls4.count(3))
cls4.clear()
# List(Array), Set, Tuple, Dictionary(Object)



