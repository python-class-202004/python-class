start = 0
# quit
# 1, condition
# 2, continue, skip current loop only
# 3, break, quit the whole loop

while start < 10:
    if start == 5:
        start += 1  # start = start + 1
        break
    print(start)
    start += 1 # start = start + 1